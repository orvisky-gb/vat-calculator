package com.example.globalblue;

import com.example.globalblue.shared.service.WebClientFactory;
import org.junit.jupiter.api.Test;
import org.springframework.web.reactive.function.client.WebClient;

import static org.junit.jupiter.api.Assertions.assertNotNull;

class WebClientFactoryTest {

    @Test
    void webClientCreationTest() {
        // Create an instance of WebClientFactory
        WebClientFactory webClientFactory = new WebClientFactory();

        // Call the webClient method to get a WebClient instance
        WebClient webClient = webClientFactory.webClient();

        assertNotNull(webClient);

    }
}
