import {Routes} from '@angular/router';
import {VatCalculatorComponent} from "./vat-calculator/vat-calculator.component";

export const routes: Routes = [
  { path: '', component: VatCalculatorComponent }
];
