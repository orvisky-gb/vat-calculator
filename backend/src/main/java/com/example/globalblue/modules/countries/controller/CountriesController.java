package com.example.globalblue.modules.countries.controller;

import com.example.globalblue.modules.countries.domain.VatLookupCountryDomain;
import com.example.globalblue.modules.countries.domain.VatLookupCountryRatesDomain;
import com.example.globalblue.modules.countries.service.CountriesService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;

@RestController
@RequestMapping(value = "countries")
@Tag(name = "countries")
public class CountriesController {

    @Autowired
    CountriesService countriesService;

    @GetMapping(value = "", produces = MediaType.APPLICATION_JSON_VALUE)
    @Operation(summary = "Countries list", tags = {"countries"})
    public @ResponseBody List<VatLookupCountryDomain> getListOfCountries() throws URISyntaxException {
        return countriesService.list();
    }

    @GetMapping(value = "/rates/{countryCode}", produces = MediaType.APPLICATION_JSON_VALUE)
    @Operation(summary = "Country rates", tags = {"countries"})
    public @ResponseBody VatLookupCountryRatesDomain getVatRatesOfCountry(@PathVariable String countryCode) throws URISyntaxException {
        return countriesService.vatRates(countryCode);
    }

}
