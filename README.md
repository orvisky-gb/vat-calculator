# VAT calculator

This project features a standalone VAT calculator component built with Angular 17 in the frontend and a Spring Boot 3.x application in the backend. Comprehensive testing covers both the frontend and backend functionalities.

Users can select a country and its corresponding VAT rates. The countries and their rates are dynamically loaded from the backend through https://vatlookup.eu/.

When a user inputs values into any one of the three fields - Net Amount, VAT Amount, or Gross Amount - the remaining fields are automatically calculated.

The user interface (UI) is implemented using Angular Material, providing a modern and consistent design for a seamless user experience.

![Alt Text](https://i.ibb.co/1nVPzKw/frontned-vat-calculator.png)

## Frontend

### Angular 17 App

#### Project Structure

- **src/app/vat-calculator**: Standalone Angular component for VAT calculations.
- **src/app/shared**: Possible shared stuff.
- **cypress**: Cypress end-to-end tests.
- **coverage**: Code coverage reports.

#### Installation

```bash
cd frontend/vat-project
npm install
```

#### Development Server

```bash
npm run start
```

Visit [http://localhost:4200/](http://localhost:4200/) in your browser.

#### Cypress e2e tests

```bash
cd frontend/vat-project
npm run e2e
```

#### Unit Tests

```bash
cd frontend/vat-project
npm run test
```

#### Code Coverage

Code coverage reports are available in the `coverage` directory.

```bash
cd frontend/vat-project
npm run test
```

Open `coverage/vat-project/index.html`

````
=============================== Coverage summary ===============================
Statements   : 100% ( 72/72 )
Branches     : 100% ( 18/18 )
Functions    : 100% ( 23/23 )
Lines        : 100% ( 65/65 )
================================================================================
````

#### Source Map Size

Source map size information can be found in the build output.

```bash
cd frontend/vat-project
npm run build:sourcemap
```

Open `dist/vat-project/browsers/index.html`

#### TODO

- [ ] Implement more unit test data and various countries.
- [ ] Implement more cypress test.
- [ ] Optimize source map size.

## Backend

Backend use public VAT API from website https://vatlookup.eu/ and runs on port 3000.

### Spring Boot 3.2.3 App

#### Project Structure

- **src/main/java/com/example/globalblue/**: Backend source code.
- **src/main/java/com/example/globalblue/configuration**: Web security configuration
- **src/main/java/com/example/globalblue/modules/countries**: Countries module
- **src/main/java/com/example/globalblue/shared/**: Shared stuff
- **src/test/java**: Unit tests for the backend.

#### Installation

```bash
cd backend
./mvnw install
```

#### Run

Run [GlobalblueApplication.java](backend%2Fsrc%2Fmain%2Fjava%2Fcom%2Fexample%2Fglobalblue%2FGlobalblueApplication.java)

```bash
cd backend
./mvnw spring-boot:run
```

The backend will start at [http://localhost:8080/](http://localhost:8080/).

#### Unit Tests

```bash
cd backend
./mvnw test
```

#### Code Coverage

Code coverage reports are available in the `target` directory.

```bash
cd backend
./mvnw test
```

Open `target/site/jacoco/index.html`

![Alt Text](https://i.ibb.co/xMFYy14/image.png)

#### TODO

- [ ] Add OpenAPI / Swagger documentation of endpoints
- [ ] Code coverage CountryService
- [ ] Dockerize together with frontend
- [ ] Enhance unit tests and add more test cases.
- [ ] Consider implementing security measures.

## Contribution

Feel free to contribute to this project by submitting issues or pull requests.

## License

This project is licensed under the [MIT License](LICENSE).
```