package com.example.globalblue;

import com.example.globalblue.modules.countries.domain.VatLookupCountryRateDomain;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;


public class VatLookupCountryRateDomainTest {

    @Test
    public void testGettersAndSetters() {
        // Arrange
        String testName = "Reduced";
        List<Integer> testRates = List.of(10, 13);

        // Act
        VatLookupCountryRateDomain countryRate = new VatLookupCountryRateDomain();
        countryRate.setName(testName);
        countryRate.setRates(testRates);

        // Assert
        assertThat(countryRate.getName()).isEqualTo(testName);
        assertThat(countryRate.getRates()).isEqualTo(testRates);
    }

    @Test
    public void testNoArgsConstructor() {
        // Act
        VatLookupCountryRateDomain countryRate = new VatLookupCountryRateDomain();

        // Assert
        assertThat(countryRate).isNotNull();
    }

    @Test
    public void testAllArgsConstructor() {
        // Arrange
        String testName = "Reduced";
        List<Integer> testRates = List.of(10, 13);

        // Act
        VatLookupCountryRateDomain countryRate = new VatLookupCountryRateDomain(testName, testRates);

        // Assert
        assertThat(countryRate.getName()).isEqualTo(testName);
        assertThat(countryRate.getRates()).isEqualTo(testRates);
    }
}
