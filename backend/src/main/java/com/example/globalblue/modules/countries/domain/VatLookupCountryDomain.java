package com.example.globalblue.modules.countries.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class VatLookupCountryDomain {
    @JsonProperty("code")
    String code;
    @JsonProperty("vat_prefix")
    String vatPrefix;
    @JsonProperty("name")
    String name;
}
