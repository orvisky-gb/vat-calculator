import {SpinnerComponent} from "./spinner.component";
import {MatDialog} from "@angular/material/dialog";
import {Injectable} from "@angular/core";

@Injectable({
  providedIn: 'root'
})
export class SpinnerService {
  private dialogRef: any;

  constructor(private dialog: MatDialog) {}

  show(): void {
    this.dialogRef = this.dialog.open(SpinnerComponent, {
      disableClose: true,
      panelClass: 'spinner-panel'
    });
  }

  hide(): void {
    if (this.dialogRef) {
      this.dialogRef.close();
    }
  }
}
