// country.service.ts

import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {CountryRateGroupModel} from "./country.model";

@Injectable({
  providedIn: 'root',
})
export class CountryService {

  constructor(private http: HttpClient) {}

  getCountries(): Observable<any> {
    return this.http.get('http://localhost:3000/countries');
  }

  getVatRates(countryCode: string): Observable<CountryRateGroupModel> {
    return this.http.get<CountryRateGroupModel>(`http://localhost:3000/countries/rates/${countryCode}`);
  }
}
