package com.example.globalblue;

import com.example.globalblue.modules.countries.domain.VatLookupCountryDomain;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;


public class VatLookupCountryDomainTest {

    @Test
    public void testGettersAndSetters() {
        // Arrange
        String testName = "Name";
        String testCode = "Code";
        String testVatPrefix = "VatPrefix";

        // Act
        VatLookupCountryDomain countryDomain = new VatLookupCountryDomain();
        countryDomain.setName(testName);
        countryDomain.setCode(testCode);
        countryDomain.setVatPrefix(testVatPrefix);

        // Assert
        assertThat(countryDomain.getName()).isEqualTo(testName);
        assertThat(countryDomain.getCode()).isEqualTo(testCode);
        assertThat(countryDomain.getVatPrefix()).isEqualTo(testVatPrefix);
    }

    @Test
    public void testNoArgsConstructor() {
        // Act
        VatLookupCountryDomain countryDomain = new VatLookupCountryDomain();

        // Assert
        assertThat(countryDomain).isNotNull();
    }

    @Test
    public void testAllArgsConstructor() {
        // Arrange
        String testName = "Name";
        String testCode = "Code";
        String testVatPrefix = "VatPrefix";

        // Act
        VatLookupCountryDomain countryDomain = new VatLookupCountryDomain(testCode, testVatPrefix, testName);

        // Assert
        assertThat(countryDomain.getName()).isEqualTo(testName);
        assertThat(countryDomain.getCode()).isEqualTo(testCode);
        assertThat(countryDomain.getVatPrefix()).isEqualTo(testVatPrefix);
    }
}
