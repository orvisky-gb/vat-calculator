import {Component} from '@angular/core';
import {AbstractControl, FormBuilder, FormGroup, ReactiveFormsModule, ValidatorFn, Validators} from "@angular/forms";
import {MatError, MatFormFieldModule} from "@angular/material/form-field";
import {CommonModule} from "@angular/common";
import {MatInputModule} from "@angular/material/input";
import {MatCardModule} from "@angular/material/card";
import {CountryService} from "../shared/country.service";
import {HttpClientModule} from "@angular/common/http";
import {SpinnerService} from "../shared/spinner/spinner.service";
import {finalize, map, Observable} from "rxjs";
import {MatSelectModule} from "@angular/material/select";
import {MatToolbarModule} from "@angular/material/toolbar";
import {CountryRateModel} from "../shared/country.model";

@Component({
  selector: 'app-vat-calculator',
  standalone: true,
  imports: [
    CommonModule,
    MatCardModule,
    MatFormFieldModule,
    MatInputModule,
    MatError,
    ReactiveFormsModule,
    HttpClientModule,
    MatSelectModule,
    MatToolbarModule
  ],
  providers: [
    CountryService,
    SpinnerService
  ],
  templateUrl: './vat-calculator.component.html',
  styleUrl: './vat-calculator.component.scss'
})
export class VatCalculatorComponent {
  vatForm!: FormGroup;
  countries$!: Observable<any[]>;
  vatRates$!: Observable<CountryRateModel[]>;

  constructor(private fb: FormBuilder, private countryService: CountryService, private spinnerService: SpinnerService) {
  }

  ngOnInit() {
    this.initializeForm()
    this.loadCountries();
  }

  private initializeForm() {
    this.vatForm = this.fb.group({
      // @todo not necessary but default based on home work
      country: ['AT', [Validators.required]],
      vatRate: [{value: null, disabled: true}, [Validators.required]],
      netAmount: [{value: null, disabled: true}, [Validators.pattern(/^\d*\.?\d+$/)]],
      grossAmount: [{value: null, disabled: true}, [Validators.pattern(/^\d*\.?\d+$/)]],
      vatAmount: [{value: null, disabled: true}, [Validators.pattern(/^\d*\.?\d+$/)]],
    }, { validators: this.atLeastOneRequired });
  }

  private atLeastOneRequired: ValidatorFn = (control: AbstractControl): { [key: string]: boolean } | null => {
    const netAmount = control.get('netAmount')?.value;
    const grossAmount = control.get('grossAmount')?.value;
    const vatAmount = control.get('vatAmount')?.value;

    if (!netAmount && !grossAmount && !vatAmount) {
      return { 'atLeastOneRequired': true };
    }

    return null;
  };

  loadCountries() {
    this.spinnerService.show();

    this.disableVatFields();

    this.countries$ = this.countryService.getCountries().pipe(
      finalize(() => {
        this.spinnerService.hide();

        if (this.vatForm.get('country')?.value) {
          this.loadVatRates();
        }
      })
    );
  }

  loadVatRates() {
    this.spinnerService.show();

    this.disableVatFields();

    this.vatRates$ = this.countryService.getVatRates(this.vatForm.get('country')?.value).pipe(
      map((countryRateGroup) => countryRateGroup.rates),
      finalize(() => {
        this.spinnerService.hide();
        this.resetVatFields();
        this.vatForm.get('vatRate')?.enable();
      })
    );
  }

  vatRatesChanged() {
    this.enableVatFields();

    if (this.vatForm.get('netAmount')?.value) {
      this.vatValuesChanged('netAmount');
    } else if (this.vatForm.get('vatAmount')?.value) {
      this.vatValuesChanged('vatAmount');
    } else if (this.vatForm.get('grossAmount')?.value) {
      this.vatValuesChanged('grossAmount');
    }
  }

  vatValuesChanged(formFieldName: string) {
    const netAmountControl = this.vatForm.get('netAmount');
    const grossAmountControl = this.vatForm.get('grossAmount');
    const vatAmountControl = this.vatForm.get('vatAmount');
    const vatRateControl = this.vatForm.get('vatRate');

    if (!netAmountControl?.value && !vatAmountControl?.value && !grossAmountControl?.value) {
      return;
    }

    switch (formFieldName) {
      case 'netAmount':
        const calculatedVatAmount = netAmountControl?.value * vatRateControl?.value * 0.01;
        vatAmountControl?.setValue(calculatedVatAmount);
        grossAmountControl?.setValue(netAmountControl?.value + calculatedVatAmount);
        break;
      case 'grossAmount':
        const calculatedNetAmount = Math.round(grossAmountControl?.value / (1 + vatRateControl?.value * 0.01) * 100) / 100;
        netAmountControl?.setValue(calculatedNetAmount);
        vatAmountControl?.setValue(grossAmountControl?.value - calculatedNetAmount);
        break;
      case 'vatAmount':
        const calculatedNetAmountFromVat = vatAmountControl?.value / (vatRateControl?.value / 100);
        netAmountControl?.setValue(calculatedNetAmountFromVat);
        grossAmountControl?.setValue(netAmountControl?.value + vatAmountControl?.value);
        break;
    }
  }

  public resetVatFields() {
    ['netAmount', 'grossAmount', 'vatAmount'].forEach(field => this.vatForm.get(field)?.reset());
  }

  private enableVatFields() {
    ['netAmount', 'grossAmount', 'vatAmount'].forEach(field => this.vatForm.get(field)?.enable());
  }

  private disableVatFields() {
    ['netAmount', 'grossAmount', 'vatAmount'].forEach(field => this.vatForm.get(field)?.disable());
  }

}
