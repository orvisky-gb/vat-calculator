package com.example.globalblue.modules.countries.service;

import com.example.globalblue.modules.countries.domain.VatLookupCountryDomain;
import com.example.globalblue.modules.countries.domain.VatLookupCountryRatesDomain;
import com.example.globalblue.shared.service.WebClientFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

@Service
public class CountriesService {

    @Autowired
    WebClientFactory webClientFactory;

    public List<VatLookupCountryDomain> list() throws URISyntaxException {

        return webClientFactory.webClient()
                .post()
                .uri(new URI("https://api.vatlookup.eu/countrylist/"))
                .retrieve()
                .bodyToFlux(VatLookupCountryDomain.class)
                .collectList()
                .block();

    }

    public VatLookupCountryRatesDomain vatRates(String countryCode) throws URISyntaxException {

        return webClientFactory.webClient()
                .post()
                .uri(new URI("https://api.vatlookup.eu/rates/" + countryCode))
                .retrieve()
                .bodyToMono(VatLookupCountryRatesDomain.class)
                .block();

    }

}
