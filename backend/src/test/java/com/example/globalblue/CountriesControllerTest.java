package com.example.globalblue;

import com.example.globalblue.modules.countries.controller.CountriesController;
import com.example.globalblue.modules.countries.domain.VatLookupCountryDomain;
import com.example.globalblue.modules.countries.domain.VatLookupCountryRateDomain;
import com.example.globalblue.modules.countries.domain.VatLookupCountryRatesDomain;
import com.example.globalblue.modules.countries.service.CountriesService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Collections;
import java.util.List;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(CountriesController.class)
class CountriesControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private CountriesService countriesService;

    @Test
    void shouldReturnListOfCountries() throws Exception {
        // Arrange
        VatLookupCountryDomain country = new VatLookupCountryDomain("code", "vatPrefix", "name");
        when(countriesService.list()).thenReturn(Collections.singletonList(country));

        // Act & Assert
        mockMvc.perform(get("/countries").contentType("application/json"))
                .andExpect(status().isOk())
                .andExpect(content().json("[{ \"code\": \"code\", \"vat_prefix\": \"vatPrefix\", \"name\": \"name\" }]"));
    }

    @Test
    void shouldReturnVatRatesForCountry() throws Exception {
        // Arrange
        String countryCode = "US";
        VatLookupCountryRatesDomain vatRates = new VatLookupCountryRatesDomain(List.of(new VatLookupCountryRateDomain("name", List.of(1, 2))), "disclaimer");
        when(countriesService.vatRates(countryCode)).thenReturn(vatRates);

        // Act & Assert
        mockMvc.perform(get("/countries/rates/{countryCode}", countryCode).contentType("application/json"))
                .andExpect(status().isOk())
                .andExpect(content().json("{\"rates\":[{\"name\":\"name\",\"rates\":[1,2]}],\"disclaimer\":\"disclaimer\"}"));

    }
}