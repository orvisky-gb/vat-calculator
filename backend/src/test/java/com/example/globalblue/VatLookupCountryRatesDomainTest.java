package com.example.globalblue;

import com.example.globalblue.modules.countries.domain.VatLookupCountryRateDomain;
import com.example.globalblue.modules.countries.domain.VatLookupCountryRatesDomain;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;


public class VatLookupCountryRatesDomainTest {

    @Test
    public void testGettersAndSetters() {
        // Arrange
        List<VatLookupCountryRateDomain> testRates = List.of(new VatLookupCountryRateDomain("name", List.of(1, 2)));
        String testDisclaimer = "disclaimer";

        // Act
        VatLookupCountryRatesDomain countryRatesDomain = new VatLookupCountryRatesDomain();
        countryRatesDomain.setRates(testRates);
        countryRatesDomain.setDisclaimer(testDisclaimer);

        // Assert
        assertThat(countryRatesDomain.getRates()).isEqualTo(testRates);
        assertThat(countryRatesDomain.getDisclaimer()).isEqualTo(testDisclaimer);
    }

    @Test
    public void testNoArgsConstructor() {
        // Act
        VatLookupCountryRatesDomain countryRatesDomain = new VatLookupCountryRatesDomain();

        // Assert
        assertThat(countryRatesDomain).isNotNull();
    }

    @Test
    public void testAllArgsConstructor() {
        // Arrange
        List<VatLookupCountryRateDomain> testRates = List.of(new VatLookupCountryRateDomain("name", List.of(1, 2)));
        String testDisclaimer = "disclaimer";

        // Act
        VatLookupCountryRatesDomain countryRatesDomain = new VatLookupCountryRatesDomain(testRates, testDisclaimer);

        // Assert
        assertThat(countryRatesDomain.getRates()).isEqualTo(testRates);
        assertThat(countryRatesDomain.getDisclaimer()).isEqualTo(testDisclaimer);
    }
}
