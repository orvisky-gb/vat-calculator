import {Component} from '@angular/core';
import {MatProgressSpinnerModule} from "@angular/material/progress-spinner";

@Component({
  selector: 'app-spinner',
  imports: [
    MatProgressSpinnerModule
  ],
  standalone: true,
  template: `
    <div class="spinner-overlay">
      <div class="spinner-container">
        <mat-spinner></mat-spinner>
      </div>
    </div>
  `,
  styleUrls: ['./spinner.component.scss']
})
export class SpinnerComponent {}
