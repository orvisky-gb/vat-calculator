package com.example.globalblue;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GlobalblueApplication {

	public static void main(String[] args) {
		SpringApplication.run(GlobalblueApplication.class, args);
	}

}
