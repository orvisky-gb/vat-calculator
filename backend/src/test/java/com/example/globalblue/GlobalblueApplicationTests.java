package com.example.globalblue;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest
class GlobalblueApplicationTests {

	@Autowired
	private GlobalblueApplication globalblueApplication;

	@Test
	void contextLoads() {
		// Ensure that the application context loads successfully
		assertNotNull(globalblueApplication);
	}

}
