export interface CountryRateGroupModel {
  rates: CountryRateModel[];
  disclaimer: string;
}

export interface CountryRateModel {
  name: string;
  rates: number[];
}
