package com.example.globalblue.modules.countries.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class VatLookupCountryRatesDomain {
    private List<VatLookupCountryRateDomain> rates;
    private String disclaimer;
}
