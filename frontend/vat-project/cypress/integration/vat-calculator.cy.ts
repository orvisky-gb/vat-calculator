// @todo only one country AT
// @todo load data from fixtures (JSON)
describe('VAT Calculator test', () => {

  beforeEach(() => {
    cy.visit('http://localhost:4200/');
  });

  it('should calculate AT VAT correctly netAMount', () => {
    cy.get('[formControlName="country"]')
      .click()
      .get('mat-option')
      .contains('Austria')
      .click();

    cy.get('[formControlName="vatRate"]')
      .click()
      .get('mat-option')
      .contains('10%')
      .click();

    cy.get('[formControlName="netAmount"]').type('100');
    cy.get('[formControlName="vatAmount"]').should('have.value', '10');
    cy.get('[formControlName="grossAmount"]').should('have.value', '110');
  });

  it('should calculate AT VAT correctly vatAMount', () => {
    cy.get('[formControlName="country"]')
      .click()
      .get('mat-option')
      .contains('Austria')
      .click();

    cy.get('[formControlName="vatRate"]')
      .click()
      .get('mat-option')
      .contains('10%')
      .click();

    cy.get('[formControlName="vatAmount"]').type('10');
    cy.get('[formControlName="netAmount"]').should('have.value', '100');
    cy.get('[formControlName="grossAmount"]').should('have.value', '110');
  });

  it('should calculate AT VAT correctly grossAmount', () => {
    cy.get('[formControlName="country"]')
      .click()
      .get('mat-option')
      .contains('Austria')
      .click();

    cy.get('[formControlName="vatRate"]')
      .click()
      .get('mat-option')
      .contains('10%')
      .click();

    cy.get('[formControlName="grossAmount"]').type('110');
    cy.get('[formControlName="vatAmount"]').should('have.value', '10');
    cy.get('[formControlName="netAmount"]').should('have.value', '100');
  });

  it('should calculate AT VAT with vatAmount error', () => {
    cy.get('[formControlName="country"]')
      .click()
      .get('mat-option')
      .contains('Austria')
      .click();

    cy.get('[formControlName="vatRate"]')
      .click()
      .get('mat-option')
      .contains('10%')
      .click();

    cy.get('[formControlName="netAmount"]').click();
    cy.get('[formControlName="vatAmount"]').click();

    cy.get('mat-card')
      .find('mat-error')
      .contains("At least one of Net Amount, Gross Amount, or VAT Amount is required.")
      .should('be.visible');
  });
})
