import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';
import {ReactiveFormsModule} from '@angular/forms';
import {MatCardModule} from '@angular/material/card';
import {MatError, MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {MatSelectModule} from '@angular/material/select';
import {MatToolbarModule} from '@angular/material/toolbar';
import {CommonModule} from '@angular/common';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {VatCalculatorComponent} from './vat-calculator.component';
import {CountryService} from '../shared/country.service';
import {SpinnerService} from '../shared/spinner/spinner.service';
import {NoopAnimationsModule} from "@angular/platform-browser/animations";
import {HttpClient} from "@angular/common/http";

describe('VatCalculatorComponent', () => {
  let component: VatCalculatorComponent;
  let fixture: ComponentFixture<VatCalculatorComponent>;
  let countryServiceSpy: jasmine.SpyObj<CountryService>;
  let spinnerServiceSpy: jasmine.SpyObj<SpinnerService>;
  let httpTestingController: HttpTestingController;
  let httpClientSpy: jasmine.SpyObj<HttpClient>;

  beforeEach(() => {
    countryServiceSpy = jasmine.createSpyObj('CountryService', ['getCountries', 'getVatRates']);
    spinnerServiceSpy = jasmine.createSpyObj('SpinnerService', ['show', 'hide']);

    TestBed.configureTestingModule({
      imports: [
        CommonModule,
        MatCardModule,
        MatFormFieldModule,
        MatInputModule,
        MatError,
        ReactiveFormsModule,
        HttpClientTestingModule,
        MatSelectModule,
        MatToolbarModule,
        NoopAnimationsModule
      ],
      providers: [
        {provide: HttpClient, useValue: httpClientSpy},
        {provide: CountryService, useValue: countryServiceSpy},
        {provide: SpinnerService, useValue: spinnerServiceSpy},
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(VatCalculatorComponent);
    component = fixture.componentInstance;
    httpTestingController = TestBed.inject(HttpTestingController);
  });

  afterEach(() => {
    httpTestingController.verify();
    fixture.destroy();
  });

  it('should create the component', () => {
    expect(component).toBeTruthy();
  });

  // @todo better http mocking ? and subscriptions change?
  // @todo fakeAsync XHR problem
  it('should initialize the form and load countries', waitForAsync(() => {
    fixture.detectChanges();

    fixture.whenStable().then(() => {
      fixture.detectChanges();
      component.countries$.subscribe((countries) => {
        expect(countries.length).toBeGreaterThan(0);
      });
    });
  }));

  // @todo avoid nested subscribe()
  it('should choose country and show vat rates', waitForAsync(() => {
    spyOn(component, 'loadVatRates').and.callThrough();
    spyOn(component, 'vatRatesChanged').and.callThrough();

    fixture.detectChanges();

    fixture.whenStable().then(() => {
      fixture.detectChanges();
      component.countries$.subscribe((countries) => {

        expect(countries.length).toBeGreaterThan(0);
        component.vatForm.get('country')?.setValue(countries[0].code);
        expect(component.vatForm.get('country')).toBeTruthy();

        fixture.detectChanges();

        component.loadVatRates();

        fixture.whenStable().then(() => {

          component.vatRates$.subscribe((vatRates) => {
            expect(vatRates.length).toBeGreaterThan(0);

            console.log(vatRates);

            // @todo fixed rate
            component.vatForm.get('vatRate')?.setValue(10);
            expect(component.vatForm.get('vatRate')).toBeTruthy();

            fixture.detectChanges();

            component.vatRatesChanged();

            expect(component.vatForm.get('netAmount')?.enabled).toBeTruthy();
            expect(component.vatForm.get('grossAmount')?.enabled).toBeTruthy();
            expect(component.vatForm.get('vatAmount')?.enabled).toBeTruthy();

            expect(component.vatRatesChanged).toHaveBeenCalled();
            expect(component.loadVatRates).toHaveBeenCalled();

          });

        });

      });
    });
  }));

  it('should switch correctly vatRatesChanged', waitForAsync(() => {
    spyOn(component, 'vatRatesChanged').and.callThrough();

    fixture.detectChanges();

    fixture.whenStable().then(() => {

      // @todo fixed rate
      component.vatForm.get('vatRate')?.setValue(10);

      component.vatForm.get('netAmount')?.setValue(100);
      component.vatRatesChanged();
      expect(component.vatForm.get('vatAmount')?.value).toEqual(10);
      expect(component.vatForm.get('grossAmount')?.value).toEqual(110);
      component.resetVatFields();

      component.vatForm.get('vatAmount')?.setValue(200);
      component.vatRatesChanged();
      expect(component.vatForm.get('netAmount')?.value).toEqual(2000);
      expect(component.vatForm.get('grossAmount')?.value).toEqual(2200);
      component.resetVatFields()

      component.vatForm.get('grossAmount')?.setValue(4400);
      component.vatRatesChanged();
      expect(component.vatForm.get('netAmount')?.value).toEqual(4000);
      expect(component.vatForm.get('vatAmount')?.value).toEqual(400);
      component.resetVatFields()

      expect(component.vatRatesChanged).toHaveBeenCalled();

    });
  }));

  it('should calculcate values correctly vatValuesChanged', waitForAsync(() => {
    spyOn(component, 'vatValuesChanged').and.callThrough();

    fixture.detectChanges();

    fixture.whenStable().then(() => {

      // @todo fixed rate
      component.vatForm.get('vatRate')?.setValue(10);

      component.vatForm.get('netAmount')?.setValue(100);
      component.vatValuesChanged('netAmount');
      expect(component.vatForm.get('vatAmount')?.value).toEqual(10);
      expect(component.vatForm.get('grossAmount')?.value).toEqual(110);

      component.vatForm.get('vatAmount')?.setValue(200);
      component.vatValuesChanged('vatAmount');
      expect(component.vatForm.get('netAmount')?.value).toEqual(2000);
      expect(component.vatForm.get('grossAmount')?.value).toEqual(2200);

      component.vatForm.get('grossAmount')?.setValue(4400);
      component.vatValuesChanged('grossAmount');
      expect(component.vatForm.get('netAmount')?.value).toEqual(4000);
      expect(component.vatForm.get('vatAmount')?.value).toEqual(400);

      expect(component.vatValuesChanged).toHaveBeenCalled();

    });
  }));

  it('should skip calculcate values correctly vatValuesChanged', waitForAsync(() => {
    spyOn(component, 'vatValuesChanged').and.callThrough();

    fixture.detectChanges();

    fixture.whenStable().then(() => {

      component.vatValuesChanged('netAmount');
      expect(component.vatForm.get('netAmount')?.value).toEqual(null);
      expect(component.vatForm.get('vatAmount')?.value).toEqual(null);
      expect(component.vatForm.get('grossAmount')?.value).toEqual(null);

      component.vatValuesChanged('vatAmount');
      expect(component.vatForm.get('netAmount')?.value).toEqual(null);
      expect(component.vatForm.get('vatAmount')?.value).toEqual(null);
      expect(component.vatForm.get('grossAmount')?.value).toEqual(null);

      component.vatValuesChanged('grossAmount');
      expect(component.vatForm.get('netAmount')?.value).toEqual(null);
      expect(component.vatForm.get('vatAmount')?.value).toEqual(null);
      expect(component.vatForm.get('grossAmount')?.value).toEqual(null);

      expect(component.vatValuesChanged).toHaveBeenCalled();

    });
  }));

  /*
  it('should load VAT rates and disable VAT fields', fakeAsync(() => {
    component.ngOnInit();

    countryServiceSpy.getVatRates.and.returnValue(of({rates: [], disclaimer: ''}));

    component.loadVatRates();

    tick();

    expect(countryServiceSpy.getVatRates).toHaveBeenCalled();
    expect(spinnerServiceSpy.show).toHaveBeenCalled();
    expect(spinnerServiceSpy.hide).toHaveBeenCalled();
    expect(component.vatForm.get('vatRate')?.enabled).toBeTrue();
  }));
   */

  // Add more test cases for other methods as needed

  afterEach(() => {
    fixture.destroy();
  });
});
